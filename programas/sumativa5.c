#include <stdio.h>
#include <string.h> /* gets, strcmp */
#include <stdlib.h> /* system */

#define PALABRA "panama"

#define LONGITUD 10

int main(){
    char palabra[LONGITUD + 1];
    int intentos = 0;
    int ingresa = 0; //
    do {
    system("cls");
    printf("\n\t\t\tESCRIBA LA PALABRA\n");
    printf("\t\t\t------------\n");
    printf("\n\tPALABRA: ");
    gets(palabra);

    if (strcmp(palabra, PALABRA) == 0 ){
        ingresa = 1;
    }else{
        printf("\n\tpalabra es incorrecta\n");
        intentos++;
        getchar();
    }
    
    
    }while (intentos < 2 && ingresa == 0);
    

    if (ingresa == 1){
        printf("\n\tFELICITACIONES\n");

    }else{ 
        printf("\n\tHA SOBREPASADO EL NUMERO DE INTENTOS\n");
    }
    return 0;
}
