#include <stdio.h>
#include <math.h>

int main(){
    float a, b, c, d;

    printf("\necuacion de segundo grado\n\n");
    printf("  2\n");
    printf("ax + bx + c = 0 \n\n");
    printf("introdusca valor de a: ");
    scanf("%f", &a);
    printf("introdusca valor de b: ");
    scanf("%f", &b);
    printf("introdusca valor de c: ");
    scanf("%f", &c);

     d=b*b-(4*a*c);

     if (d==0)
     printf("sol.: %.2f\n", -b/(2*a));
     else if(d>0){
         printf("sol. 1: %.2f\n", (-b+sqrt(d))/(2*a));
         printf("sol. 2: %.2f\n", (-b-sqrt(d))/(2*a));
     }
     else { /* d<0 */
     printf("sol. 1: %.2f+%.2f i\n", -b/(2*a), (sqrt(-d))/(2*a));
     printf("sol. 2: %.2f-%.2f i\n", -b/(2*a), (sqrt(-d))/(2*a));
     }
      return 0;
}